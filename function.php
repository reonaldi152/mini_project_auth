<?php


$db = mysqli_connect("localhost", "root", "", "tahupalingtahu");

function query($query)
{
    global $db;
    $result = mysqli_query($db, $query);
    $array = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $array[] = $row;
    }

    return $array;
}

// untuk tambah produk
function tambah($data)
{
    global $db;
    $nama = htmlspecialchars($data["nama"]);
    $deskripsi = htmlspecialchars($data["deskripsi"]);
    $harga = htmlspecialchars($data["harga"]);
    $gambar = htmlspecialchars($data["gambar"]);
    $rating = htmlspecialchars($data["rating"]);
    $tipe = htmlspecialchars($data["tipe"]);


    // upload gambar
    $gambar = upload();
    if (!$gambar) {
        return false;
    }

    // query insert data
    $query = "INSERT INTO produk VALUES('','$nama', '$deskripsi', '$harga', '$gambar','$rating','$tipe',)";
    mysqli_query($db, $query);
    // setelah dijalankan mysqli_query, si function tambah mengembalikkan angka, angka yang di dapat dr mysqli_affected_rows
    return mysqli_affected_rows($db);
}

function hapus($id)
{
    global $db;
    mysqli_query($db, "DELETE FROM produk WHERE id = $id");

    return mysqli_affected_rows($db);
}

function update($data)
{
    global $db;
    // ambil data dari tiap elemen dalam form
    $id = $data["id"];
    $nama = htmlspecialchars($data["nama"]);
    $deskripsi = htmlspecialchars($data["deskripsi"]);
    $harga = htmlspecialchars($data["harga"]);
    $gambarLama = htmlspecialchars($data["gambarLama"]);
    $rating = htmlspecialchars($data["rating"]);
    $tipe = htmlspecialchars($data["tipe"]);

    // cek apakah user pilih gambar baru atau tidak
    if ($_FILES['gambar']['error'] === 4) {
        $gambar = $gambarLama;
    } else {
        $gambar = upload();
    }

    // query insert data
    $query = "UPDATE produk SET
                nama = '$nama',
                deskripsi = '$deskripsi',
                harga = '$harga',
                gambar = '$gambar',
                rating = '$rating',
                tipe = '$tipe'
                WHERE id = $id";
    mysqli_query($db, $query);
    // setelah dijalankan mysqli_query, si function tambah mengembalikkan angka, angka yang di dapat dr mysqli_affected_rows
    return mysqli_affected_rows($db);
}

function cari($keyword)
{
    $query =  "SELECT * FROM produk WHERE nama LIKE '%$keyword%' OR harga LIKE '%$keyword%'";
    return query($query);
}

function upload()
{
    // ambil dlu isi $_FILES
    $namaFile = $_FILES['gambar']['name'];

    $ukuranFile = $_FILES['gambar']['size'];

    // untuk mengetahui ada gambar yang di upload atau tidak
    $error = $_FILES['gambar']['error'];

    // tempat penyimpanan sementara
    $tmpName = $_FILES['gambar']['tmp_name'];

    // cek apakah tidak ada gambar di upload
    // didalam array gambar lalu array error, jika int(4), maka gambar atau file belum di upload
    if ($error === 4) {

        echo "<script>
                alert('pilih gambar terlebih dahulu');
            </script>";

        return false;
    }

    // mengecek yang di upload itu gambar atau bukan
    // cek apakah yang diupload adalah gambar

    $ekstensiGambarValid = ['jpg', 'jpeg', 'png', 'svg'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));
    // exploade itu sebuah fungsi untuk memecah string menjadi array
    // semua ekstensi harus kecil semua hurufnya,
    //kemudian ekstensi yang diambil ke dalam $ekstensiGambar adalah array yang terakhir maka dari itu menggunakan end()


    if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
        // in_array buat ngecek adakah sebuah string di dalam sebuah array

        echo "<script>
                alert('yang anda upload bukan gambar');
            </script>";

        return false;
    }

    // cek jika ukurannya yang diupload terlalu besar, dalam ukuran byte
    if ($ukuranFile > 2000000) {
        echo "<script>
                alert('ukuran gambar terlalu besar');
            </script>";

        return false;
    }


    // generate nama gambar baru
    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;

    // uniqid() akan membangkitkan string random

    move_uploaded_file($tmpName, 'img/' . $namaFileBaru);

    return $namaFileBaru;
    // mengapa di return, supaya  $gambar yang diatas di //upload gambar keisi
}


function registrasi($data)
{
    global $db;

    $nama = htmlspecialchars(($data["nama"]));
    $email = htmlspecialchars(($data["email"]));
    // $username = strtolower(stripslashes($data["username"]));
    $password =  mysqli_real_escape_string($db, $data["password"]);
    $password2 = mysqli_real_escape_string($db, $data["password2"]);
    $address = htmlspecialchars(($data["address"]));
    $no_rumah = htmlspecialchars(($data["no_rumah"]));
    $no_telp = htmlspecialchars(($data["no_telp"]));
    $kota = htmlspecialchars(($data["kota"]));
    $status = htmlspecialchars(($data["status"]));

    $token = md5($email . $password);


    // cek email sudah ada atau belum
    $result = mysqli_query($db, "SELECT email FROM users WHERE email = '$email'");

    if (mysqli_fetch_assoc($result)) {
        echo "<script>
            alert('Email sudah terdaftar');
        </script>";

        return false;
    }

    // cek konfirmasi password
    if ($password !== $password2) {
        echo "<script>
            alert('konfirmasi password tidak sesuai');
        </script>";

        return false;
    }

    // enkripsi password

    //parameter password_hash ada dua, password apa yg mau diacak, acaknya pake algoritma apa
    $password = password_hash($password, PASSWORD_DEFAULT);
    // var_dump($password); die;

    // tambahkan user baru ke database
    mysqli_query($db, "INSERT INTO users VALUES('','$nama','$email','$password','$address','$no_rumah','$no_telp','$kota','$token', '$status')");

    return mysqli_affected_rows($db);
}
